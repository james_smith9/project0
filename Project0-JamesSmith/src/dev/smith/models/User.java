package dev.smith.models;
import java.util.ArrayList;

public class User {
	
	UserType type;
	private static long nextID = 0;
	
	private long uID;
	private String username, password, transactionHistory = ""; //Not sure if password should a getter. I don't the idea of another part of code grabbing user passwords. Will attempt to work around this.
	public ArrayList<BankAccount> accts;
	
	public User(){
		type = UserType.CUSTOMER;
		uID = User.nextID++;
		accts = new ArrayList<BankAccount>();
	}
	
	public User(String username, String password)
	{
		type = UserType.CUSTOMER;
		uID = User.nextID++;
		this.username = username;
		this.password = password;
		accts = new ArrayList<BankAccount>();
	}
	
	public void deposit(double d, int acct){
		accts.get(acct).deposit(d);
		transactionHistory += "User " + username + " deposited $" + d + "\n";
	}
	
	public double withdraw(double d, int acct){
		transactionHistory += "User " + username + " withdrew $" + d + "\n";
		return accts.get(acct).withdraw(d);
	}
	
	public void addAccount()
	{
		accts.add(new BankAccount(this));
	}
	
	public void addAccount(double initialDeposit)
	{
		addAccount();
		deposit(initialDeposit, accts.size()-1);
	}
	
	
	public long getuID() {
		return uID;
	}

	public void setuID(long uID) {
		this.uID = uID;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}
	
	void printTransactionHistory(){
		System.out.println(transactionHistory);
	}

	@Override
	public String toString() {
		return "User [type=" + type + ", uID=" + uID + ", username=" + username + ", password=" + password
				+ ", transactionHistory=" + transactionHistory + ", accts=" + accts + "]";
	}


	
}
