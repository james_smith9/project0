package dev.smith.models;

public class BankAccount {
	
	private String accountID, username;
	private double accountBalance;
	
	BankAccount(){
		accountID = "N/A";
		accountBalance = 0.0;
		username = "";
	}
	
	BankAccount(User newUser){
		accountID = "" + newUser.getuID() + "-" + newUser.accts.size();
		username = newUser.getUsername();
	}
	
	void deposit(double d){
		accountBalance += d;
	};
	
	double withdraw(double d){
		accountBalance -= d; 
		return d;
	};
	
	double getBalance(){
		return accountBalance;
	}

	@Override
	public String toString() {
		return "BankAccount [accountID=" + accountID + ", username=" + username + ", accountBalance=" + accountBalance
				+ "]";
	};
	


	
}
