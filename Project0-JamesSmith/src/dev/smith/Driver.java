package dev.smith;

import dev.smith.models.*;

//import java.sql.*; Need to figure out how this works

public class Driver {
	public static void main(String[] args) {
		User user1 = new User("James", "password");
		user1.addAccount(5000);
		user1.addAccount(1000);
		User user2 = new User("Smith", "gamenight");
		user2.addAccount(300);
		System.out.println(user1);
		System.out.println(user2);
	}
}
